Compile:

1. Please go to project folder which contain pom.xml
   $ cd ./LanguageModelNLP

2. Compile maven project
   $ mvn package

3. Usage
The command line of this program need two variable first one is query string,
the second one is lambda for smooting.
eg. java -jar ./target/LanguageModel-0.0.1-SNAPSHOT.jar "information retrieval" 0.5


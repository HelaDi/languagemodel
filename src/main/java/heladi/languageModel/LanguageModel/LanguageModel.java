package heladi.languageModel.LanguageModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

public class LanguageModel{
	private HashMap<String, Integer> termsInCollection;
	private ArrayList<HashMap<String, Integer>> collection;
	private ArrayList<Integer> documentsLengths;
	private int collectionTokensNum;
	private StanfordCoreNLP pipeline;
	private Properties props;
	
	public LanguageModel(){
		termsInCollection = new HashMap<String, Integer>();
		collection = new ArrayList<HashMap<String, Integer>>();
		documentsLengths = new ArrayList<Integer>();
		collectionTokensNum = 0;
		props = new Properties();
    props.setProperty("annotators", "tokenize, ssplit, pos, lemma");
    pipeline = new StanfordCoreNLP(props, false);
	}
	
	public int getCollectionSize(){
		return collection.size();
	}
	
	public void setADocument(String doc){
		Annotation annotation = pipeline.process(doc);
		String tempLemma = "";
		HashMap<String, Integer> document = new HashMap<String, Integer>();
		int documentLength = 0;
		for(CoreMap sentence : annotation.get(SentencesAnnotation.class)){
			for(CoreLabel token : sentence.get(TokensAnnotation.class)){
				tempLemma = token.get(LemmaAnnotation.class);
				if(document.containsValue(tempLemma)){
					termsInCollection.put(tempLemma, termsInCollection.get(tempLemma)+1);
					document.put(tempLemma, document.get(tempLemma)+1);
				}else{
					termsInCollection.put(tempLemma, 1);
					document.put(tempLemma, 1);
				}
				documentLength++;
			}
		}
		collection.add(document);
		documentsLengths.add(documentLength);
		collectionTokensNum += documentLength;
	}
	
	public float QueryGivenDocumentProbability(int docID, String query){
		String[] strings = query.split("[^\\w']+");
		float tempProbability = 1;
		float tempTermFrequency = 0;
		int documentLength = documentsLengths.get(docID);
		for(int i = 0; i < strings.length; i++){
			if(collection.get(docID).containsKey(strings[i])){
				tempTermFrequency = collection.get(docID).get(strings[i]);
				tempProbability = tempProbability * (tempTermFrequency / documentLength);
			}else
				return 0;
		}
		return tempProbability;
	}
	
	public float QueryGivenDocumentProbabilityJMSmoothing(int docID, String query, float lambda){
		String[] strings = query.split("[^\\w']+");
		float tempProbability = 1;
		float tempDocTermFrequency = 0;
		float documentLength = documentsLengths.get(docID);
		float tempColTermFrequency = 0;
		for(int i = 0; i < strings.length; i++){
			if(collection.get(docID).containsKey(strings[i])){
				tempDocTermFrequency = collection.get(docID).get(strings[i]);
				tempColTermFrequency = termsInCollection.get(strings[i]);
				tempProbability = tempProbability * (lambda * tempProbability * (tempDocTermFrequency / documentLength) 
						+ (1 - lambda) * (tempColTermFrequency / collectionTokensNum));
			}else{
				tempProbability = tempProbability * (1 - lambda) * (tempColTermFrequency / collectionTokensNum);
			}
		}
		return tempProbability;
	}
}

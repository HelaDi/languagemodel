package heladi.languageModel.LanguageModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public class LanguageModelNLP {
	 public static void main(String[] args){
		 if(args.length == 0){
				System.err.println("Please input query and lambda!");
				return;
			}

			LanguageModel languageModel = new LanguageModel();
			languageModel.setADocument("information retrieval is the most awesome class I ever took.");
			languageModel.setADocument("the retrieval of private information from your emails is a job that the NSA loves.");
			languageModel.setADocument("in the school of information you can learn about data science.");
			languageModel.setADocument("the labrador retriever is a great dog.");
			System.out.println("---------------------------------------------");
			System.out.println("Documents' score without smoothing");
			ArrayList<DocumentScore> scoreList = new ArrayList<DocumentScore>();
			float tempScore = 0;
			DocumentScore tempDocumentScore = null;
			for(int i = 0; i < languageModel.getCollectionSize(); i++){
				tempScore = languageModel.QueryGivenDocumentProbability(i, args[0]);
				tempDocumentScore = new DocumentScore(i+1, tempScore);
				scoreList.add(tempDocumentScore);
			}
			Collections.sort(scoreList, new DocumentScore());
			System.out.println("Document ID		   Score");
			for(int i = 0; i < scoreList.size(); i++){
				System.out.format("%d                          %f%n", scoreList.get(i).getDocID(), scoreList.get(i).getScore());
			}
			
			System.out.println("---------------------------------------------");
			System.out.println("Documents' score with Jeline-Mercer smoothing");
			scoreList.clear();
			tempScore = 0;
			tempDocumentScore = null;
			for(int i = 0; i < languageModel.getCollectionSize(); i++){
				tempScore = languageModel.QueryGivenDocumentProbabilityJMSmoothing(i, args[0], Float.parseFloat(args[1]));
				tempDocumentScore = new DocumentScore(i+1, tempScore);
				scoreList.add(tempDocumentScore);
			}
			Collections.sort(scoreList, new DocumentScore());
			System.out.println("Document ID		   Score");
			for(int i = 0; i < scoreList.size(); i++){
				System.out.format("%d                          %f%n", scoreList.get(i).getDocID(), scoreList.get(i).getScore());
			}
	}
}
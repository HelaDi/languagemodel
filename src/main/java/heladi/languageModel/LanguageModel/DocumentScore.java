package heladi.languageModel.LanguageModel;

import java.util.Comparator;

public class DocumentScore implements Comparator<DocumentScore>{
	private int docID;
	private float score;
	
	public DocumentScore(){
		docID = 0;
		score = 0;
	}
	
	public DocumentScore(int docID, float score){
		this.docID = docID;
		this.score = score;
	}
	
	void setDocID(int docID){
		this.docID = docID;
	}
	
	int getDocID(){
		return docID;
	}
	
	void setScore(float score){
		this.score = score;
	}
	
	float getScore(){
		return score;
	}

	public int compare(DocumentScore d1, DocumentScore d2) {
		return -Float.compare(d1.score, d2.score);
	}
}
